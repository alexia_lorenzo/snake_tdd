/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _greenfoot_h
#define _greenfoot_h

#include "jni_inout.h"

typedef jobject World;
typedef jobject Board;
typedef jobject Actor;
typedef jobject Piece;
typedef jobject Color;

//////////////////////////////////////////////////////
//           Global Variables operations            //
//////////////////////////////////////////////////////

#define setGlobalWorld(object, name, world)       setGlobal_(object, name, world, __FILE__, __FUNCTION__, __LINE__)

#define getGlobalWorld(object, name)              getGlobal_(object, name, __FILE__, __FUNCTION__, __LINE__)

#define setGlobalBoard(object, name, board)       setGlobal_(object, name, board, __FILE__, __FUNCTION__, __LINE__)

#define getGlobalBoard(object, name)              getGlobal_(object, name, __FILE__, __FUNCTION__, __LINE__)

#define setGlobalActor(object, name, actor)       setGlobal_(object, name, actor, __FILE__, __FUNCTION__, __LINE__)

#define getGlobalActor(object, name)              getGlobal_(object, name, __FILE__, __FUNCTION__, __LINE__)

#define setGlobalPiece(object, name, piece)       setGlobal_(object, name, piece, __FILE__, __FUNCTION__, __LINE__)

#define getGlobalPiece(object, name)              getGlobal_(object, name, __FILE__, __FUNCTION__, __LINE__)

#define setGlobalColor(object, name, color)       setGlobal_(object, name, color, __FILE__, __FUNCTION__, __LINE__)

#define getGlobalColor(object, name)              getGlobal_(object, name, __FILE__, __FUNCTION__, __LINE__)


//////////////////////////////////////////////////////
//                  Game operations                 //
//////////////////////////////////////////////////////

#define setWorld(world)            setWorld_(world, __FILE__, __FUNCTION__, __LINE__)

#define getKey()                   getKey_(__FILE__, __FUNCTION__, __LINE__)

#define isKeyDown(key)             isKeyDown_(key, __FILE__, __FUNCTION__, __LINE__)

#define delay(time)                delay_(time, __FILE__, __FUNCTION__, __LINE__)

#define setSpeed(speed)            setSpeed(speed, __FILE__, __FUNCTION__, __LINE__)

#define stopScenario()             stopScenario_(__FILE__, __FUNCTION__, __LINE__)

#define startScenario()            startScenario_(__FILE__, __FUNCTION__, __LINE__)

#define getRandomNumber(limit)     getRandomNumber_(limit, __FILE__, __FUNCTION__, __LINE__)

#define playSound(sound)           playSound_(sound, __FILE__, __FUNCTION__, __LINE__)

#define mousePressed(actor)        mousePressed_(actor, __FILE__, __FUNCTION__, __LINE__)

#define mouseClicked(actor)        mouseClicked_(actor, __FILE__, __FUNCTION__, __LINE__)

#define mouseDragged(actor)        mouseDragged_(actor, __FILE__, __FUNCTION__, __LINE__)

#define mouseDragEnded(actor)      mouseDragEnded_(actor, __FILE__, __FUNCTION__, __LINE__)

#define mouseMoved(actor)          mouseMoved_(actor, __FILE__, __FUNCTION__, __LINE__)

#define getMicLevel()              getMicLevel_(__FILE__, __FUNCTION__, __LINE__)

#define ask(prompt)                ask(prompt, __FILE__, __FUNCTION__, __LINE__)


//////////////////////////////////////////////////////
//                 Color operations                 //
//////////////////////////////////////////////////////

#define newColor(r, g, b, name)      newColor_(r, g, b, name, __FILE__, __FUNCTION__, __LINE__)

#define toColorName(color)           toColorName_(color, __FILE__, __FUNCTION__, __LINE__)

#define toColor(colorName)           toColor_(colorName, __FILE__, __FUNCTION__, __LINE__)


//////////////////////////////////////////////////////
//                 World operations                 //
//////////////////////////////////////////////////////

#define newWorld(type)                                         newWorld_(type, __FILE__, __FUNCTION__, __LINE__)

#define getWorldType(world)                                    getWorldType_(world, __FILE__, __FUNCTION__, __LINE__)

#define getWidth(world)                                        getWidth_(world, __FILE__, __FUNCTION__, __LINE__)

#define getHeight(world)                                       getHeight_(world, __FILE__, __FUNCTION__, __LINE__)

#define getCellSize(world)                                     getCellSize_(world, __FILE__, __FUNCTION__, __LINE__)

#define getBackgroundFile(world)                               getBackgroundFile_(world, __FILE__, __FUNCTION__, __LINE__)

#define setBackgroundFile(world, imageFileName)                setBackgroundFile_(world, imageFileName, __FILE__, __FUNCTION__, __LINE__)

#define addActorToWorld(world, actor, x, y)                    addActorToWorld_(world, actor, x, y, __FILE__, __FUNCTION__, __LINE__)

#define getActors(world, actorType, actorsNumber)              getActors_(world, actorType, actorsNumber, __FILE__, __FUNCTION__, __LINE__)

#define getActorsNumber(world, actorType)                      getActorsNumber_(world, actorType, __FILE__, __FUNCTION__, __LINE__)

#define getActorsAt(world, x, y, actorType, actorsNumber)      getActorsAt_(world, x, y, actorType, actorsNumber, __FILE__, __FUNCTION__, __LINE__)

#define getActorsNumberAt(world, x, y, actorType)              getActorsNumberAt_(world, x, y, actorType, __FILE__, __FUNCTION__, __LINE__)

#define getActOrderActors(world, actOrderActorsNumber)         getActOrderActors_(world, actOrderActorsNumber, __FILE__, __FUNCTION__, __LINE__)

#define setActOrderActors(world, actors, actorsNumber)         setActOrderActors_(world, actors, actorsNumber, __FILE__, __FUNCTION__, __LINE__)

#define setPaintOrder(world, actors, actorsNumber)             setPaintOrder_(world, actors, actorsNumber, __FILE__, __FUNCTION__, __LINE__)

#define getColorAt(world, x, y)                                getColorAt_(world, x, y, __FILE__, __FUNCTION__, __LINE__)

#define removeActor(world, actor)                              removeActor_(world, actor, __FILE__, __FUNCTION__, __LINE__)

#define removeActors(world, actors, actorsNumber)              removeActors_(world, actors, actorsNumber, __FILE__, __FUNCTION__, __LINE__)

#define repaint(world)                                         repaint_(world, __FILE__, __FUNCTION__, __LINE__)

#define showText(world, text, x, y)                            showText_(world, text, x, y, __FILE__, __FUNCTION__, __LINE__)


//////////////////////////////////////////////////////
//                 Actor operations                 //
//////////////////////////////////////////////////////

#define newActor(type)                        newActor_(type, __FILE__, __FUNCTION__, __LINE__)

#define getActorType(actor)                   getActorType_(actor, __FILE__, __FUNCTION__, __LINE__)

#define getImageFile(actor)                   getImageFile_(actor, __FILE__, __FUNCTION__, __LINE__)

#define getImageWidth(actor)                  getImageWidth_(actor, __FILE__, __FUNCTION__, __LINE__)

#define getImageHeight(actor)                 getImageHeight_(actor, __FILE__, __FUNCTION__, __LINE__)

#define setImageFile(actor, imageFileName)    setImageFile_(actor, imageFileName, __FILE__, __FUNCTION__, __LINE__)

#define setImageScale(actor, width, height)   setImageScale_(actor, width, height, __FILE__, __FUNCTION__, __LINE__)

#define getWorld(actor)                       getWorld_(actor, __FILE__, __FUNCTION__, __LINE__)

#define getX(actor)                           getX_(actor, __FILE__, __FUNCTION__, __LINE__)

#define getY(actor)                           getY_(actor, __FILE__, __FUNCTION__, __LINE__)

#define getRotation(actor)                    getRotation_(actor, __FILE__, __FUNCTION__, __LINE__)

#define setLocation(actor, x, y)              setLocation_(actor, x, y, __FILE__, __FUNCTION__, __LINE__)

#define setRotation(actor, angle)             setRotation_(actor, angle, __FILE__, __FUNCTION__, __LINE__)

#define move(actor, distance)                 move_(actor, distance, __FILE__, __FUNCTION__, __LINE__)

#define turn(actor, angle)                    turn_(actor, angle, __FILE__, __FUNCTION__, __LINE__)

#define turnTorward(actor, angle)             turnTorward_(actor, angle, __FILE__, __FUNCTION__, __LINE__)

#define isTouching(actor, actorType)          isTouching_(actor, actorType, __FILE__, __FUNCTION__, __LINE__)

#define removeTouching(actor, actorType)      removeTouching_(actor, actorType, __FILE__, __FUNCTION__, __LINE__)

#define isAtEdge(actor)                       isAtEdge_(actor, __FILE__, __FUNCTION__, __LINE__)


//////////////////////////////////////////////////////
//                Board operations                  //
//////////////////////////////////////////////////////

#define newBoard(type)                                                                  newBoard_(type, __FILE__, __FUNCTION__, __LINE__)

#define getBoardType(board)                                                             getBoardType_(board, __FILE__, __FUNCTION__, __LINE__)

#define setCellColor(board, x, y, color)                                                setCellColor_(board, x, y, color, __FILE__, __FUNCTION__, __LINE__)

#define setCellColorWithBorder(board, x, y, borderColor, insideColor)                   setCellColorWithBorder_(board, x, y, borderColor, insideColor, __FILE__, __FUNCTION__, __LINE__)

#define getCellBorderColor(board, x, y)                                                 getCellBorderColor_(board, x, y, __FILE__, __FUNCTION__, __LINE__)

#define getCellInsideColor(board, x, y)                                                 getCellInsideColor_(board, x, y, __FILE__, __FUNCTION__, __LINE__)

#define setBackgroundColor(board, color)                                                setBackgroundColor_(board, color, __FILE__, __FUNCTION__, __LINE__)

#define setBackgroundColorWithBorder(board, borderColor, insideColor)                   setBackgroundColorWithBorder_(board, borderColor, insideColor, __FILE__, __FUNCTION__, __LINE__)

#define setBoardBackground(board, cellColor, rows, columns)                             setBoardBackground_(board, cellColor, rows, columns, __FILE__, __FUNCTION__, __LINE__)

#define setBoardBackgroundWithBorder(board, borderColor, cellColor, rows, columns)      setBoardBackgroundWithBorder_(board, borderColor, cellColor, rows, columns, __FILE__, __FUNCTION__, __LINE__)

#define getBoardBackground(board, colors, colorsLength)                                 getBoardBackground_(board, colors, colorsLength,  __FILE__, __FUNCTION__, __LINE__)

#define setTurnColor(board, turnColor)                                                  setTurnColor_(board, turnColor, __FILE__, __FUNCTION__, __LINE__)

#define getTurnColor(board)                                                             getTurnColor_(board, __FILE__, __FUNCTION__, __LINE__)

#define thereIsSelectedPiece(board)                                                     thereIsSelectedPiece_(board, __FILE__, __FUNCTION__, __LINE__)

#define getSelectedPiece(board)                                                         getSelectedPiece_(board, __FILE__, __FUNCTION__, __LINE__)

#define thereIsPartialMovement(board)                                                   thereIsPartialMovement_(board, __FILE__, __FUNCTION__, __LINE__)

#define getMovedPiece(board)                                                            getMovedPiece_(board, __FILE__, __FUNCTION__, __LINE__)

#define getMovedPieceX(board)                                                           getMovedPieceX_(board, __FILE__, __FUNCTION__, __LINE__)

#define getMovedPieceY(board)                                                           getMovedPieceY_(board, __FILE__, __FUNCTION__, __LINE__)

#define getPiece(board, x, y)                                                           getPiece_(board, x, y, __FILE__, __FUNCTION__, __LINE__)

#define piecesNumber(board)                                                             piecesNumber_(board, __FILE__, __FUNCTION__, __LINE__)

#define piecesNumberAt(board, x, y)                                                     piecesNumberAt_(board, x, y, __FILE__, __FUNCTION__, __LINE__)

#define thereIsPieceAt(board, x, y)                                                     thereIsPieceAt_(board, x, y, __FILE__, __FUNCTION__, __LINE__)

#define thereIsColorPieceAt(board, x, y, color)                                         thereIsColorPieceAt_(board, x, y, color, __FILE__, __FUNCTION__, __LINE__)

#define canBeSelected(board, piece)                                                     canBeSelected_(board, piece, __FILE__, __FUNCTION__, __LINE__)

#define setSelectedPiece(board, piece)                                                  setSelectedPiece_(board, piece, __FILE__, __FUNCTION__, __LINE__)

#define setNullSelectedPiece(board)                                                     setNullSelectedPiece_(board, __FILE__, __FUNCTION__, __LINE__)

#define setBoardPieces(board, pieces)                                                   setBoardPieces_(board, pieces, __FILE__, __FUNCTION__, __LINE__)

#define getBoardPieces(board)                                                           getBoardPieces_(board, __FILE__, __FUNCTION__, __LINE__)

#define getBoardSquares(board)                                                          getBoardSquares_(board, __FILE__, __FUNCTION__, __LINE__)

#define setGreenSquareAt(board, x, y)                                                   setGreenSquareAt_(board, x, y, __FILE__, __FUNCTION__, __LINE__)

#define setGreenSquareWithEatenPiecesAt(board, x, y, eatenPieces, eatenPiecesNumber)    setGreenSquareWithEatenPiecesAt_(board, x, y, eatenPieces, eatenPiecesNumber, __FILE__, __FUNCTION__, __LINE__)

#define setYellowSquareAt(board, x, y)                                                  setYellowSquareAt_(board, x, y, __FILE__, __FUNCTION__, __LINE__)

#define setYellowSquareWithNextPieceAt(board, x, y, nextPiece)                          setYellowSquareWithNextPieceAt_(board, x, y, nextPiece, __FILE__, __FUNCTION__, __LINE__)


//////////////////////////////////////////////////////
//                Piece operations                  //
//////////////////////////////////////////////////////

#define newPiece(type)                            newPiece_(type, __FILE__, __FUNCTION__, __LINE__)

#define getPieceType(piece)                       getPieceType_(piece, __FILE__, __FUNCTION__, __LINE__)

#define addPieceToBoard(board, piece, x, y)       addActorToWorld_(board, piece, x, y, __FILE__, __FUNCTION__, __LINE__)


//////////////////////////////////////////////////////
//       Simulation operations for Testing          //
//////////////////////////////////////////////////////

#define newGenericWorld(width, height, cellSize)    newGenericWorld_(width, height, cellSize, __FILE__, __FUNCTION__, __LINE__); //++++++++

#define runOnce()                                   runOnce_(__FILE__, __FUNCTION__, __LINE__)

#define runOnceWorld(world)                         runOnceWorld_(world, __FILE__, __FUNCTION__, __LINE__)

#define runOnceActors(actors, actorsLength)         runOnceActors_(actors, actorsLength, __FILE__, __FUNCTION__, __LINE__)

#define runOnceWorldWithdActors(world, actors)      runOnceWorldWithdActors_(world, actor, __FILE__, __FUNCTION__, __LINE__)

#define isWorldStarted()                            isWorldStarted_(__FILE__, __FUNCTION__, __LINE__)

#define isWorldStopped()                            isWorldStopped_(__FILE__, __FUNCTION__, __LINE__)

#define isSpeed(speed)                              isSpeed_(speed, __FILE__, __FUNCTION__, __LINE__)

#define getSpeed()                                  getSpeed_(__FILE__, __FUNCTION__, __LINE__)

#define isDelay(delay)                              isDelay_(delay, __FILE__, __FUNCTION__, __LINE__)

#define getDelay()                                  getDelay_(__FILE__, __FUNCTION__, __LINE__)

#define getPlayedSound()                            getPlayedSound_(__FILE__, __FUNCTION__, __LINE__)

#define isPlayed(sound)                             isPlayed_(sound, __FILE__, __FUNCTION__, __LINE__)

#define typeKey(key)                                typeKey_(key, __FILE__, __FUNCTION__, __LINE__)

#define pressKey(key)                               pressKey_(key, __FILE__, __FUNCTION__, __LINE__)

#define releaseKey(key)                             releaseKey_(key, __FILE__, __FUNCTION__, __LINE__)

#define clickMouse(x, y)                            clickMouse_(x, y, __FILE__, __FUNCTION__, __LINE__)

#define pressMouse(x, y)                            pressMouse_(x, y, __FILE__, __FUNCTION__, __LINE__)

#define dragMouse(x, y)                             dragMouse_(x, y, __FILE__, __FUNCTION__, __LINE__)

#define releaseMouse(x, y)                          releaseMouse_(x, y, __FILE__, __FUNCTION__, __LINE__)

#define moveMouse(x, y)                             moveMouse_(x, y, __FILE__, __FUNCTION__, __LINE__)

#define setRandom(seed)                             setRandom_(seed, __FILE__, __FUNCTION__, __LINE__)

#define setAsk(nextAsk)                             setAsk_(nextAsk, __FILE__, __FUNCTION__, __LINE__)

#include "greenfoot.c"
#endif
